const Generator = require('../../abstractGenerator');
const {arrayTimesArray, sum, randRange, diffYears} = require('../../helpers');
const handleParams = require('../../handleParams');

module.exports = class PESEL extends Generator {
    static get weights() {
        return [1, 3, 7, 9, 1, 3, 7, 9, 1, 3];
    }

    params() {
        return {
            birthdate: {
                type: 'string',
                pattern: '^(18|19|20|21|22)[0-9][0-9](-[0-9][0-9])?(-[0-9][0-9])?$',
            },
            gender: {
                type: 'string',
                values: ['m', 'f', 'mf'],
                default: 'mf',
            },
        };
    }

    generate(params = {}) {
        params = handleParams(this.params(), params);

        let [y, m, d] = this._parseDateRequirement(params.birthdate);
        if (!y) {
            y = randRange(1900, 2020);
        }
        if (!m) {
            m = randRange(1, 13);
        }
        if (!d) {
            d = randRange(1, 29);
        }

        if (y < 1900) {
            m += 80;
        }

        if (2000 <= y && y < 2100) {
            m += 20;
        }

        if (2100 <= y && y < 2200) {
            m += 40;
        }

        if (2200 <= y && y < 2300) {
            m += 60;
        }

        const serial = randRange(0, 1000);

        const gender = params.gender;

        const genderDigit = gender === 'mf'
            ? randRange(0, 10)
            : 2 * randRange(0, 5) + (gender === 'm' ? 1 : 0);

        const nr = y.toString().slice(2, 4)
            + m.toString().padStart(2, '0')
            + d.toString().padStart(2, '0')
            + serial.toString().padStart(3, '0')
            + genderDigit;

        const checksum = (10 - sum(arrayTimesArray(PESEL.weights, nr)) % 10) % 10;

        return nr + checksum;
    }

    validate(value) {
        if (!value.match(/^\d{11}$/g)) {
            throw new Error('format')
        }

        let [y, m, d, c] = [parseInt(value.slice(0, 2)), parseInt(value.slice(2, 4)), parseInt(value.slice(4, 6)), 19];

        if (m > 80) {
            m -= 80;
            c = 18;
        }

        if (20 < m && m < 40) {
            m -= 20;
            c = 20;
        }

        if (40 < m && m < 60) {
            m -= 40;
            c = 21;

        }

        if (60 < m && m < 80) {
            m -= 60;
            c = 22;
        }

        if ((m > 12) || (d > 31) || (m === 0) || (d === 0)) {
            throw new Error('birthdate')
        }

        if ((10 - sum(arrayTimesArray(PESEL.weights, value.slice(0, 10))) % 10) % 10 !== parseInt(value[10])) {
            throw new Error('checksum')
        }

        const birthdate = new Date(`${c * 100 + y}-${m.toString().padStart(2, '0')}-${d.toString().padStart(2, '0')}T00:00:00Z`);

        return {
            birthdate: birthdate,
            age: diffYears(new Date(), birthdate),
            gender: parseInt(value[9]) % 2 ? 'm' : 'f',
        };
    }

    _parseDateRequirement(date) {
        if (date === '') {
            return [undefined, undefined, undefined];
        }

        if (date.match(/^\d{4}$/g)) {
            return [parseInt(date), undefined, undefined];
        }

        if (date.match(/^\d{4}-\d{2}$/g)) {
            return [parseInt(date.slice(0, 4)), parseInt(date.slice(5, 7)), undefined];
        }

        if (date.match(/^\d{4}-\d{2}-\d{2}$/g)) {
            return [parseInt(date.slice(0, 4)), parseInt(date.slice(5, 7)), parseInt(date.slice(8, 10))];
        }

        throw new Error('Invalid date requirement');
    }
};
